# # CreateTokenResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token_type** | **string** | Type of token to access the api, in our case always Bearer | 
**expires_in** | **int** | Token lifetime in seconds | 
**access_token** | **string** | Access token in JWT format | 
**refresh_token** | **string** | Token by which a new access token can be obtained | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


