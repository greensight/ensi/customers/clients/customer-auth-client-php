# # ConfirmationCodeResponseData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | Error code | [optional] 
**message** | **string** | Error message | 
**seconds** | **int** | Number of seconds | 
**is_exist** | **bool** | User registered | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


