# # SearchConfirmationCodesFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int[]** | Identifiers of verification code | [optional] 
**user_id** | **int[]** | Identifiers of user | [optional] 
**phone** | **string** | Phone number | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


