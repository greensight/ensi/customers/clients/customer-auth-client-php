# # User

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | User Identifier | 
**confirmation_code** | **string** | Confirmation code | 
**password_token** | **string** | Token | 
**created_at** | [**\DateTime**](\DateTime.md) | Registration date | 
**updated_at** | [**\DateTime**](\DateTime.md) | Update date | 
**active** | **bool** | Active | [optional] 
**login** | **string** | Login | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


