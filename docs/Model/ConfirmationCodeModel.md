# # ConfirmationCodeModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Identifier of verification code | 
**user_id** | **int** | Identifier of user | 
**phone** | **string** | Phone number | 
**attempt** | **int** | Request verification code attempt number | 
**failed_checks_count** | **int** | Number of errors when entering the verification code | 
**expired_at** | [**\DateTime**](\DateTime.md) | Expiration date of the verification code | 
**verified_at** | [**\DateTime**](\DateTime.md) | Verification date (correct code entry) | 
**created_at** | [**\DateTime**](\DateTime.md) | Date of creation | 
**updated_at** | [**\DateTime**](\DateTime.md) | Date of updation | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


