# # SearchConfirmationCodesResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\CustomerAuthClient\Dto\ConfirmationCodeModel[]**](ConfirmationCodeModel.md) |  | 
**meta** | [**\Ensi\CustomerAuthClient\Dto\SearchUsersResponseMeta**](SearchUsersResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


