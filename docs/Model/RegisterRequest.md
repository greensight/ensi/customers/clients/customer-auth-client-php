# # RegisterRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**validation_type** | **string** | Type of validation (by password or confirmation code) | [optional] 
**login** | **string** | Login (specified as email) | 
**password** | **string** | Password | [optional] 
**code** | **string** | Confirmation code | [optional] 
**first_name** | **string** | First name | 
**last_name** | **string** | Surname | [optional] 
**phone** | **string** | Phone number | 
**birthday** | **string** | Birthday in the format yyyyy-mm-dd | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


