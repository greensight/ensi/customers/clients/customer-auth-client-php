# # PasswordResetSecondRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reset_type** | **string** | Password reset method (phone/email) | [optional] 
**code** | **string** | Confirmation code or password-token | [optional] 
**email** | **string** | User Email | [optional] 
**phone** | **string** | User phone number | [optional] 
**password** | **string** | Password | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


