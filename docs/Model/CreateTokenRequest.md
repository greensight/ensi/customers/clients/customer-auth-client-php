# # CreateTokenRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**grant_type** | **string** | Type from GrantTypeEnum | 
**client_id** | **string** | Id client application | 
**client_secret** | **string** | The secret of the client application | 
**scope** | **string[]** |  | 
**username** | **string** | User login | [optional] 
**password** | **string** | User password | [optional] 
**phone** | **string** | Phone number | [optional] 
**code** | **string** | Confirmation code | [optional] 
**refresh_token** | **string** | Update token | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


