# Ensi\CustomerAuthClient\UsersApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUser**](UsersApi.md#createUser) | **POST** /users | Creating an object of type User
[**deleteUser**](UsersApi.md#deleteUser) | **DELETE** /users/{id} | Deleting an object of type User
[**getCurrentUser**](UsersApi.md#getCurrentUser) | **POST** /users:current | Get the current user
[**getUser**](UsersApi.md#getUser) | **GET** /users/{id} | Getting an object of type User
[**passwordReset**](UsersApi.md#passwordReset) | **POST** /users/password-reset-first | Password reset (sending code)
[**passwordResetSecond**](UsersApi.md#passwordResetSecond) | **POST** /users/password-reset-second | Password reset (Second)
[**patchUser**](UsersApi.md#patchUser) | **PATCH** /users/{id} | Updating a part of fields of the User object type
[**refreshPasswordToken**](UsersApi.md#refreshPasswordToken) | **POST** /users/{id}:refresh-password-token | Updating the token to set the password
[**register**](UsersApi.md#register) | **POST** /users:register | Register a new user
[**searchUser**](UsersApi.md#searchUser) | **POST** /users:search-one | Search for an object of type User
[**searchUsers**](UsersApi.md#searchUsers) | **POST** /users:search | Search for objects of type User



## createUser

> \Ensi\CustomerAuthClient\Dto\UserResponse createUser($create_user_request)

Creating an object of type User

Creating an object of type User

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_user_request = new \Ensi\CustomerAuthClient\Dto\CreateUserRequest(); // \Ensi\CustomerAuthClient\Dto\CreateUserRequest | 

try {
    $result = $apiInstance->createUser($create_user_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->createUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_user_request** | [**\Ensi\CustomerAuthClient\Dto\CreateUserRequest**](../Model/CreateUserRequest.md)|  |

### Return type

[**\Ensi\CustomerAuthClient\Dto\UserResponse**](../Model/UserResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteUser

> \Ensi\CustomerAuthClient\Dto\EmptyDataResponse deleteUser($id)

Deleting an object of type User

Deleting an object of type User

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric id

try {
    $result = $apiInstance->deleteUser($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->deleteUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric id |

### Return type

[**\Ensi\CustomerAuthClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getCurrentUser

> \Ensi\CustomerAuthClient\Dto\UserResponse getCurrentUser()

Get the current user

Get the current user

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = Ensi\CustomerAuthClient\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getCurrentUser();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->getCurrentUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\CustomerAuthClient\Dto\UserResponse**](../Model/UserResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getUser

> \Ensi\CustomerAuthClient\Dto\UserResponse getUser($id, $include)

Getting an object of type User

Getting an object of type User

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric id
$include = 'include_example'; // string | Related entities to be loaded, comma separated

try {
    $result = $apiInstance->getUser($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->getUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric id |
 **include** | **string**| Related entities to be loaded, comma separated | [optional]

### Return type

[**\Ensi\CustomerAuthClient\Dto\UserResponse**](../Model/UserResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## passwordReset

> \Ensi\CustomerAuthClient\Dto\UserResponse passwordReset($password_reset_first_request)

Password reset (sending code)

Password reset (sending confirmation code to mail/phone number)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$password_reset_first_request = new \Ensi\CustomerAuthClient\Dto\PasswordResetFirstRequest(); // \Ensi\CustomerAuthClient\Dto\PasswordResetFirstRequest | 

try {
    $result = $apiInstance->passwordReset($password_reset_first_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->passwordReset: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **password_reset_first_request** | [**\Ensi\CustomerAuthClient\Dto\PasswordResetFirstRequest**](../Model/PasswordResetFirstRequest.md)|  |

### Return type

[**\Ensi\CustomerAuthClient\Dto\UserResponse**](../Model/UserResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## passwordResetSecond

> \Ensi\CustomerAuthClient\Dto\UserResponse passwordResetSecond($password_reset_second_request)

Password reset (Second)

Password reset

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$password_reset_second_request = new \Ensi\CustomerAuthClient\Dto\PasswordResetSecondRequest(); // \Ensi\CustomerAuthClient\Dto\PasswordResetSecondRequest | 

try {
    $result = $apiInstance->passwordResetSecond($password_reset_second_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->passwordResetSecond: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **password_reset_second_request** | [**\Ensi\CustomerAuthClient\Dto\PasswordResetSecondRequest**](../Model/PasswordResetSecondRequest.md)|  |

### Return type

[**\Ensi\CustomerAuthClient\Dto\UserResponse**](../Model/UserResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchUser

> \Ensi\CustomerAuthClient\Dto\UserResponse patchUser($id, $patch_user_request)

Updating a part of fields of the User object type

Updating a part of fields of the User object type

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric id
$patch_user_request = new \Ensi\CustomerAuthClient\Dto\PatchUserRequest(); // \Ensi\CustomerAuthClient\Dto\PatchUserRequest | 

try {
    $result = $apiInstance->patchUser($id, $patch_user_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->patchUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric id |
 **patch_user_request** | [**\Ensi\CustomerAuthClient\Dto\PatchUserRequest**](../Model/PatchUserRequest.md)|  |

### Return type

[**\Ensi\CustomerAuthClient\Dto\UserResponse**](../Model/UserResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## refreshPasswordToken

> \Ensi\CustomerAuthClient\Dto\EmptyDataResponse refreshPasswordToken($id)

Updating the token to set the password

Updating the token to set the password

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Numeric id

try {
    $result = $apiInstance->refreshPasswordToken($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->refreshPasswordToken: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Numeric id |

### Return type

[**\Ensi\CustomerAuthClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## register

> \Ensi\CustomerAuthClient\Dto\EmptyDataResponse register($register_request)

Register a new user

Register a new user

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$register_request = new \Ensi\CustomerAuthClient\Dto\RegisterRequest(); // \Ensi\CustomerAuthClient\Dto\RegisterRequest | 

try {
    $result = $apiInstance->register($register_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->register: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **register_request** | [**\Ensi\CustomerAuthClient\Dto\RegisterRequest**](../Model/RegisterRequest.md)|  |

### Return type

[**\Ensi\CustomerAuthClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchUser

> \Ensi\CustomerAuthClient\Dto\UserResponse searchUser($search_users_request)

Search for an object of type User

Search for an object of type User

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_users_request = new \Ensi\CustomerAuthClient\Dto\SearchUsersRequest(); // \Ensi\CustomerAuthClient\Dto\SearchUsersRequest | 

try {
    $result = $apiInstance->searchUser($search_users_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->searchUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_users_request** | [**\Ensi\CustomerAuthClient\Dto\SearchUsersRequest**](../Model/SearchUsersRequest.md)|  |

### Return type

[**\Ensi\CustomerAuthClient\Dto\UserResponse**](../Model/UserResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchUsers

> \Ensi\CustomerAuthClient\Dto\SearchUsersResponse searchUsers($search_users_request)

Search for objects of type User

Search for objects of type User

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_users_request = new \Ensi\CustomerAuthClient\Dto\SearchUsersRequest(); // \Ensi\CustomerAuthClient\Dto\SearchUsersRequest | 

try {
    $result = $apiInstance->searchUsers($search_users_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->searchUsers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_users_request** | [**\Ensi\CustomerAuthClient\Dto\SearchUsersRequest**](../Model/SearchUsersRequest.md)|  |

### Return type

[**\Ensi\CustomerAuthClient\Dto\SearchUsersResponse**](../Model/SearchUsersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

