# Ensi\CustomerAuthClient\CommonApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteCustomerPersonalData**](CommonApi.md#deleteCustomerPersonalData) | **POST** /common/customer-personal-data:delete | Deleting customer personal data in OMS



## deleteCustomerPersonalData

> \Ensi\CustomerAuthClient\Dto\EmptyDataResponse deleteCustomerPersonalData($delete_customer_personal_data_request)

Deleting customer personal data in OMS

Deleting customer personal data in OMS

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\CommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$delete_customer_personal_data_request = new \Ensi\CustomerAuthClient\Dto\DeleteCustomerPersonalDataRequest(); // \Ensi\CustomerAuthClient\Dto\DeleteCustomerPersonalDataRequest | 

try {
    $result = $apiInstance->deleteCustomerPersonalData($delete_customer_personal_data_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->deleteCustomerPersonalData: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_customer_personal_data_request** | [**\Ensi\CustomerAuthClient\Dto\DeleteCustomerPersonalDataRequest**](../Model/DeleteCustomerPersonalDataRequest.md)|  |

### Return type

[**\Ensi\CustomerAuthClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

