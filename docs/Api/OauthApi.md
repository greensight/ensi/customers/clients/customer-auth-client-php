# Ensi\CustomerAuthClient\OauthApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createConfirmationCodeByPhone**](OauthApi.md#createConfirmationCodeByPhone) | **POST** /users/confirmation-code-by-phone | Receiving a code for registration or authorisation
[**createToken**](OauthApi.md#createToken) | **POST** /oauth/token | Create a new authentication token
[**deleteToken**](OauthApi.md#deleteToken) | **DELETE** /oauth/tokens/{id} | Delete authentication token



## createConfirmationCodeByPhone

> \Ensi\CustomerAuthClient\Dto\ConfirmationCodeResponse createConfirmationCodeByPhone($create_confirmation_code_by_phone_request)

Receiving a code for registration or authorisation

Receiving a code for registration or authorisation

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\OauthApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_confirmation_code_by_phone_request = new \Ensi\CustomerAuthClient\Dto\CreateConfirmationCodeByPhoneRequest(); // \Ensi\CustomerAuthClient\Dto\CreateConfirmationCodeByPhoneRequest | 

try {
    $result = $apiInstance->createConfirmationCodeByPhone($create_confirmation_code_by_phone_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OauthApi->createConfirmationCodeByPhone: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_confirmation_code_by_phone_request** | [**\Ensi\CustomerAuthClient\Dto\CreateConfirmationCodeByPhoneRequest**](../Model/CreateConfirmationCodeByPhoneRequest.md)|  |

### Return type

[**\Ensi\CustomerAuthClient\Dto\ConfirmationCodeResponse**](../Model/ConfirmationCodeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createToken

> \Ensi\CustomerAuthClient\Dto\CreateTokenResponse createToken($create_token_request)

Create a new authentication token

Create a new authentication token

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\OauthApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_token_request = new \Ensi\CustomerAuthClient\Dto\CreateTokenRequest(); // \Ensi\CustomerAuthClient\Dto\CreateTokenRequest | 

try {
    $result = $apiInstance->createToken($create_token_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OauthApi->createToken: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_token_request** | [**\Ensi\CustomerAuthClient\Dto\CreateTokenRequest**](../Model/CreateTokenRequest.md)|  |

### Return type

[**\Ensi\CustomerAuthClient\Dto\CreateTokenResponse**](../Model/CreateTokenResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteToken

> deleteToken($id)

Delete authentication token

Delete authentication token

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = Ensi\CustomerAuthClient\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Ensi\CustomerAuthClient\Api\OauthApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = b292369ad4d10944183cc5f05c77528e3749202b9745ea0bd88f6b1e6181fff6e364bd36cd11a4ed; // string | String id

try {
    $apiInstance->deleteToken($id);
} catch (Exception $e) {
    echo 'Exception when calling OauthApi->deleteToken: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| String id |

### Return type

void (empty response body)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

