# Ensi\CustomerAuthClient\ConfirmationCodesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchConfirmationCodes**](ConfirmationCodesApi.md#searchConfirmationCodes) | **POST** /confirmation-codes:search | Search for objects of type ConfirmationCode



## searchConfirmationCodes

> \Ensi\CustomerAuthClient\Dto\SearchConfirmationCodesResponse searchConfirmationCodes($search_confirmation_codes_request)

Search for objects of type ConfirmationCode

Search for objects of type ConfirmationCode

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CustomerAuthClient\Api\ConfirmationCodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_confirmation_codes_request = new \Ensi\CustomerAuthClient\Dto\SearchConfirmationCodesRequest(); // \Ensi\CustomerAuthClient\Dto\SearchConfirmationCodesRequest | 

try {
    $result = $apiInstance->searchConfirmationCodes($search_confirmation_codes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ConfirmationCodesApi->searchConfirmationCodes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_confirmation_codes_request** | [**\Ensi\CustomerAuthClient\Dto\SearchConfirmationCodesRequest**](../Model/SearchConfirmationCodesRequest.md)|  |

### Return type

[**\Ensi\CustomerAuthClient\Dto\SearchConfirmationCodesResponse**](../Model/SearchConfirmationCodesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

