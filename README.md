# CustomerAuthClient

Customer authorisation

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 1.0.0
- Build package: org.openapitools.codegen.languages.PhpClientCodegen
For more information, please visit [https://ensi.tech/contacts](https://ensi.tech/contacts)

## Requirements

PHP 5.5 and later

## Installation & Usage

### Composer

To install the bindings via [Composer](http://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://gitlab.com/greensight/ensi/customers/clients/customer-auth-client-php.git"
    }
  ],
  "require": {
    "ensi/customer-auth-client": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
    require_once('/path/to/CustomerAuthClient/vendor/autoload.php');
```

## Tests

To run the unit tests:

```bash
composer install
./vendor/bin/phpunit
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new Ensi\CustomerAuthClient\Api\CommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$delete_customer_personal_data_request = new \Ensi\CustomerAuthClient\Dto\DeleteCustomerPersonalDataRequest(); // \Ensi\CustomerAuthClient\Dto\DeleteCustomerPersonalDataRequest | 

try {
    $result = $apiInstance->deleteCustomerPersonalData($delete_customer_personal_data_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->deleteCustomerPersonalData: ', $e->getMessage(), PHP_EOL;
}

?>
```

## Documentation for API Endpoints

All URIs are relative to *http://localhost/api/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*CommonApi* | [**deleteCustomerPersonalData**](docs/Api/CommonApi.md#deletecustomerpersonaldata) | **POST** /common/customer-personal-data:delete | Deleting customer personal data in OMS
*ConfirmationCodesApi* | [**searchConfirmationCodes**](docs/Api/ConfirmationCodesApi.md#searchconfirmationcodes) | **POST** /confirmation-codes:search | Search for objects of type ConfirmationCode
*OauthApi* | [**createConfirmationCodeByPhone**](docs/Api/OauthApi.md#createconfirmationcodebyphone) | **POST** /users/confirmation-code-by-phone | Receiving a code for registration or authorisation
*OauthApi* | [**createToken**](docs/Api/OauthApi.md#createtoken) | **POST** /oauth/token | Create a new authentication token
*OauthApi* | [**deleteToken**](docs/Api/OauthApi.md#deletetoken) | **DELETE** /oauth/tokens/{id} | Delete authentication token
*UsersApi* | [**createUser**](docs/Api/UsersApi.md#createuser) | **POST** /users | Creating an object of type User
*UsersApi* | [**deleteUser**](docs/Api/UsersApi.md#deleteuser) | **DELETE** /users/{id} | Deleting an object of type User
*UsersApi* | [**getCurrentUser**](docs/Api/UsersApi.md#getcurrentuser) | **POST** /users:current | Get the current user
*UsersApi* | [**getUser**](docs/Api/UsersApi.md#getuser) | **GET** /users/{id} | Getting an object of type User
*UsersApi* | [**passwordReset**](docs/Api/UsersApi.md#passwordreset) | **POST** /users/password-reset-first | Password reset (sending code)
*UsersApi* | [**passwordResetSecond**](docs/Api/UsersApi.md#passwordresetsecond) | **POST** /users/password-reset-second | Password reset (Second)
*UsersApi* | [**patchUser**](docs/Api/UsersApi.md#patchuser) | **PATCH** /users/{id} | Updating a part of fields of the User object type
*UsersApi* | [**refreshPasswordToken**](docs/Api/UsersApi.md#refreshpasswordtoken) | **POST** /users/{id}:refresh-password-token | Updating the token to set the password
*UsersApi* | [**register**](docs/Api/UsersApi.md#register) | **POST** /users:register | Register a new user
*UsersApi* | [**searchUser**](docs/Api/UsersApi.md#searchuser) | **POST** /users:search-one | Search for an object of type User
*UsersApi* | [**searchUsers**](docs/Api/UsersApi.md#searchusers) | **POST** /users:search | Search for objects of type User


## Documentation For Models

 - [ConfirmationCodeModel](docs/Model/ConfirmationCodeModel.md)
 - [ConfirmationCodeResponse](docs/Model/ConfirmationCodeResponse.md)
 - [ConfirmationCodeResponseData](docs/Model/ConfirmationCodeResponseData.md)
 - [CreateConfirmationCodeByPhoneRequest](docs/Model/CreateConfirmationCodeByPhoneRequest.md)
 - [CreateTokenRequest](docs/Model/CreateTokenRequest.md)
 - [CreateTokenResponse](docs/Model/CreateTokenResponse.md)
 - [CreateUserRequest](docs/Model/CreateUserRequest.md)
 - [CustomerPersonalData](docs/Model/CustomerPersonalData.md)
 - [DeleteCustomerPersonalDataRequest](docs/Model/DeleteCustomerPersonalDataRequest.md)
 - [EmptyDataResponse](docs/Model/EmptyDataResponse.md)
 - [Error](docs/Model/Error.md)
 - [ErrorResponse](docs/Model/ErrorResponse.md)
 - [ErrorResponse2](docs/Model/ErrorResponse2.md)
 - [GrantTypeEnum](docs/Model/GrantTypeEnum.md)
 - [MessageForResetPassword](docs/Model/MessageForResetPassword.md)
 - [MessageForResetPasswordSuccess](docs/Model/MessageForResetPasswordSuccess.md)
 - [MessageForSetPassword](docs/Model/MessageForSetPassword.md)
 - [PaginationTypeCursorEnum](docs/Model/PaginationTypeCursorEnum.md)
 - [PaginationTypeEnum](docs/Model/PaginationTypeEnum.md)
 - [PaginationTypeOffsetEnum](docs/Model/PaginationTypeOffsetEnum.md)
 - [PasswordResetFirstRequest](docs/Model/PasswordResetFirstRequest.md)
 - [PasswordResetSecondRequest](docs/Model/PasswordResetSecondRequest.md)
 - [PatchUserRequest](docs/Model/PatchUserRequest.md)
 - [RegisterRequest](docs/Model/RegisterRequest.md)
 - [RegisterValidationTypeEnum](docs/Model/RegisterValidationTypeEnum.md)
 - [RequestBodyCursorPagination](docs/Model/RequestBodyCursorPagination.md)
 - [RequestBodyOffsetPagination](docs/Model/RequestBodyOffsetPagination.md)
 - [RequestBodyPagination](docs/Model/RequestBodyPagination.md)
 - [ResponseBodyCursorPagination](docs/Model/ResponseBodyCursorPagination.md)
 - [ResponseBodyOffsetPagination](docs/Model/ResponseBodyOffsetPagination.md)
 - [ResponseBodyPagination](docs/Model/ResponseBodyPagination.md)
 - [SearchConfirmationCodesRequest](docs/Model/SearchConfirmationCodesRequest.md)
 - [SearchConfirmationCodesResponse](docs/Model/SearchConfirmationCodesResponse.md)
 - [SearchUsersRequest](docs/Model/SearchUsersRequest.md)
 - [SearchUsersResponse](docs/Model/SearchUsersResponse.md)
 - [SearchUsersResponseMeta](docs/Model/SearchUsersResponseMeta.md)
 - [User](docs/Model/User.md)
 - [UserFillableProperties](docs/Model/UserFillableProperties.md)
 - [UserReadonlyProperties](docs/Model/UserReadonlyProperties.md)
 - [UserResponse](docs/Model/UserResponse.md)
 - [UserWriteOnlyProperties](docs/Model/UserWriteOnlyProperties.md)


## Documentation For Authorization



## bearerAuth


- **Type**: Bearer authentication (JWT)


## Author

mail@greensight.ru

