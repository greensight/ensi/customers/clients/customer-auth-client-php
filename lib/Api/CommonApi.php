<?php
/**
 * CommonApi
 * PHP version 7.3
 *
 * @category Class
 * @package  Ensi\CustomerAuthClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi Customer Auth
 *
 * Customer authorisation
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Ensi\CustomerAuthClient\Api;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Ensi\CustomerAuthClient\ApiException;
use Ensi\CustomerAuthClient\Configuration;
use Ensi\CustomerAuthClient\HeaderSelector;
use Ensi\CustomerAuthClient\ObjectSerializer;

/**
 * CommonApi Class Doc Comment
 *
 * @category Class
 * @package  Ensi\CustomerAuthClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class CommonApi
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var Configuration
     */
    protected $config;

    /**
     * @var HeaderSelector
     */
    protected $headerSelector;

    /**
     * @var int host index
     */
    protected $hostIndex;

    /**
     * @param ClientInterface $client
     * @param Configuration   $config
     * @param HeaderSelector  $selector
     * @param int             $host_index (Optional) host index to select the list of hosts if defined in the OpenAPI spec
     */
    public function __construct(
        ClientInterface $client = null,
        Configuration $config = null,
        HeaderSelector $selector = null,
        int $host_index = 0
    ) {
        $this->client = $client ?: new Client();
        $this->config = $config ?: new Configuration();
        $this->headerSelector = $selector ?: new HeaderSelector();
        $this->hostIndex = $host_index;
    }

    /**
     * Set the host index
     */
    public function setHostIndex(int $host_index): void
    {
        $this->hostIndex = $host_index;
    }

    /**
     * Get the host index
     */
    public function getHostIndex(): int
    {
        return $this->hostIndex;
    }

    public function getConfig(): Configuration
    {
        return $this->config;
    }

    /**
     * Operation deleteCustomerPersonalData
     *
     * Deleting customer personal data in OMS
     *
     * @param  \Ensi\CustomerAuthClient\Dto\DeleteCustomerPersonalDataRequest $delete_customer_personal_data_request delete_customer_personal_data_request (required)
     *
     * @throws \Ensi\CustomerAuthClient\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return \Ensi\CustomerAuthClient\Dto\EmptyDataResponse|\Ensi\CustomerAuthClient\Dto\ErrorResponse|\Ensi\CustomerAuthClient\Dto\ErrorResponse
     */
    public function deleteCustomerPersonalData($delete_customer_personal_data_request)
    {
        list($response) = $this->deleteCustomerPersonalDataWithHttpInfo($delete_customer_personal_data_request);
        return $response;
    }

    /**
     * Operation deleteCustomerPersonalDataWithHttpInfo
     *
     * Deleting customer personal data in OMS
     *
     * @param  \Ensi\CustomerAuthClient\Dto\DeleteCustomerPersonalDataRequest $delete_customer_personal_data_request (required)
     *
     * @throws \Ensi\CustomerAuthClient\ApiException on non-2xx response
     * @throws \InvalidArgumentException
     * @return array of \Ensi\CustomerAuthClient\Dto\EmptyDataResponse|\Ensi\CustomerAuthClient\Dto\ErrorResponse|\Ensi\CustomerAuthClient\Dto\ErrorResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function deleteCustomerPersonalDataWithHttpInfo($delete_customer_personal_data_request)
    {
        $request = $this->deleteCustomerPersonalDataRequest($delete_customer_personal_data_request);

        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException(
                    "[{$e->getCode()}] {$e->getMessage()}",
                    $e->getCode(),
                    $e->getResponse() ? $e->getResponse()->getHeaders() : null,
                    $e->getResponse() ? (string) $e->getResponse()->getBody() : null
                );
            }

            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(
                    sprintf(
                        '[%d] Error connecting to the API (%s)',
                        $statusCode,
                        $request->getUri()
                    ),
                    $statusCode,
                    $response->getHeaders(),
                    $response->getBody()
                );
            }

            $responseBody = $response->getBody();
            switch($statusCode) {
                case 200:
                    if ('\Ensi\CustomerAuthClient\Dto\EmptyDataResponse' === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = (string) $responseBody;
                    }

                    return [
                        ObjectSerializer::deserialize($content, '\Ensi\CustomerAuthClient\Dto\EmptyDataResponse', []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                case 400:
                    if ('\Ensi\CustomerAuthClient\Dto\ErrorResponse' === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = (string) $responseBody;
                    }

                    return [
                        ObjectSerializer::deserialize($content, '\Ensi\CustomerAuthClient\Dto\ErrorResponse', []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                case 500:
                    if ('\Ensi\CustomerAuthClient\Dto\ErrorResponse' === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = (string) $responseBody;
                    }

                    return [
                        ObjectSerializer::deserialize($content, '\Ensi\CustomerAuthClient\Dto\ErrorResponse', []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
            }

            $returnType = '\Ensi\CustomerAuthClient\Dto\EmptyDataResponse';
            $responseBody = $response->getBody();
            if ($returnType === '\SplFileObject') {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = (string) $responseBody;
            }

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders()
            ];

        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Ensi\CustomerAuthClient\Dto\EmptyDataResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Ensi\CustomerAuthClient\Dto\ErrorResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
                case 500:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        '\Ensi\CustomerAuthClient\Dto\ErrorResponse',
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }

    /**
     * Operation deleteCustomerPersonalDataAsync
     *
     * Deleting customer personal data in OMS
     *
     * @param  \Ensi\CustomerAuthClient\Dto\DeleteCustomerPersonalDataRequest $delete_customer_personal_data_request (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function deleteCustomerPersonalDataAsync($delete_customer_personal_data_request)
    {
        return $this->deleteCustomerPersonalDataAsyncWithHttpInfo($delete_customer_personal_data_request)
            ->then(
                function ($response) {
                    return $response[0];
                }
            );
    }

    /**
     * Operation deleteCustomerPersonalDataAsyncWithHttpInfo
     *
     * Deleting customer personal data in OMS
     *
     * @param  \Ensi\CustomerAuthClient\Dto\DeleteCustomerPersonalDataRequest $delete_customer_personal_data_request (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function deleteCustomerPersonalDataAsyncWithHttpInfo($delete_customer_personal_data_request)
    {
        $returnType = '\Ensi\CustomerAuthClient\Dto\EmptyDataResponse';
        $request = $this->deleteCustomerPersonalDataRequest($delete_customer_personal_data_request);

        return $this->client
            ->sendAsync($request, $this->createHttpClientOption())
            ->then(
                function ($response) use ($returnType) {
                    $responseBody = $response->getBody();
                    if ($returnType === '\SplFileObject') {
                        $content = $responseBody; //stream goes to serializer
                    } else {
                        $content = (string) $responseBody;
                    }

                    return [
                        ObjectSerializer::deserialize($content, $returnType, []),
                        $response->getStatusCode(),
                        $response->getHeaders()
                    ];
                },
                function ($exception) {
                    if (!$exception instanceof RequestException) {
                        throw $exception;
                    }

                    $response = $exception->getResponse();
                    $statusCode = $response->getStatusCode();
                    throw new ApiException(
                        sprintf(
                            '[%d] Error connecting to the API (%s)',
                            $statusCode,
                            $exception->getRequest()->getUri()
                        ),
                        $statusCode,
                        $response->getHeaders(),
                        $response->getBody()
                    );
                }
            );
    }

    /**
     * Create request for operation 'deleteCustomerPersonalData'
     *
     * @param  \Ensi\CustomerAuthClient\Dto\DeleteCustomerPersonalDataRequest $delete_customer_personal_data_request (required)
     *
     * @throws \InvalidArgumentException
     * @return \GuzzleHttp\Psr7\Request
     */
    protected function deleteCustomerPersonalDataRequest($delete_customer_personal_data_request)
    {
        // verify the required parameter 'delete_customer_personal_data_request' is set
        if ($delete_customer_personal_data_request === null || (is_array($delete_customer_personal_data_request) && count($delete_customer_personal_data_request) === 0)) {
            throw new \InvalidArgumentException(
                'Missing the required parameter $delete_customer_personal_data_request when calling deleteCustomerPersonalData'
            );
        }

        $resourcePath = '/common/customer-personal-data:delete';
        $formParams = [];
        $queryParams = [];
        $headerParams = [];
        $httpBody = '';
        $multipart = false;



        $clientOriginalFilenamesHolder = [];
        // body params
        $_tempBody = null;
        if (isset($delete_customer_personal_data_request)) {
            $_tempBody = $delete_customer_personal_data_request;
        }

        if ($multipart) {
            $headers = $this->headerSelector->selectHeadersForMultipart(
                ['application/json']
            );
        } else {
            $headers = $this->headerSelector->selectHeaders(
                ['application/json'],
                ['application/json']
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            // $_tempBody is the method argument, if present
            if ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode(ObjectSerializer::sanitizeForSerialization($_tempBody));
            } else {
                $httpBody = $_tempBody;
            }
        } elseif (count($formParams) > 0) {
            if ($multipart) {
                $multipartContents = [];
                foreach ($formParams as $formParamName => $formParamValue) {
                    $temp = [
                        'name' => $formParamName,
                        'contents' => $formParamValue
                    ];

                    if (isset($clientOriginalFilenamesHolder[$formParamName])) {
                        $temp['filename'] = $clientOriginalFilenamesHolder[$formParamName];
                    }

                    $multipartContents[] = $temp;
                }
                // for HTTP post (form)
                $httpBody = new MultipartStream($multipartContents);

            } elseif ($headers['Content-Type'] === 'application/json') {
                $httpBody = \GuzzleHttp\json_encode($formParams);

            } else {
                // for HTTP post (form)
                $httpBody = \GuzzleHttp\Psr7\Query::build($formParams);
            }
        }


        $defaultHeaders = [];
        if ($this->config->getUserAgent()) {
            $defaultHeaders['User-Agent'] = $this->config->getUserAgent();
        }

        $headers = array_merge(
            $defaultHeaders,
            $headerParams,
            $headers
        );

        $query = \GuzzleHttp\Psr7\Query::build($queryParams);
        return new Request(
            'POST',
            $this->config->getHost() . $resourcePath . ($query ? "?{$query}" : ''),
            $headers,
            $httpBody
        );
    }

    /**
     * Create http client option
     *
     * @throws \RuntimeException on file opening failure
     * @return array of http client options
     */
    protected function createHttpClientOption(): array
    {
        $options = [];
        if ($this->config->getDebug()) {
            $options[RequestOptions::DEBUG] = fopen($this->config->getDebugFile(), 'a');
            if (!$options[RequestOptions::DEBUG]) {
                throw new \RuntimeException('Failed to open the debug file: ' . $this->config->getDebugFile());
            }
        }

        return $options;
    }
}
