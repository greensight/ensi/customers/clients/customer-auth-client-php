<?php

namespace Ensi\CustomerAuthClient;

class CustomerAuthClientProvider
{
    /** @var string[] */
    public static $apis = [
        '\Ensi\CustomerAuthClient\Api\OauthApi',
        '\Ensi\CustomerAuthClient\Api\ConfirmationCodesApi',
        '\Ensi\CustomerAuthClient\Api\UsersApi',
        '\Ensi\CustomerAuthClient\Api\CommonApi',
    ];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\CustomerAuthClient\Dto\RequestBodyCursorPagination',
        '\Ensi\CustomerAuthClient\Dto\RegisterValidationTypeEnum',
        '\Ensi\CustomerAuthClient\Dto\PaginationTypeCursorEnum',
        '\Ensi\CustomerAuthClient\Dto\ResponseBodyPagination',
        '\Ensi\CustomerAuthClient\Dto\Error',
        '\Ensi\CustomerAuthClient\Dto\CreateUserRequest',
        '\Ensi\CustomerAuthClient\Dto\ResponseBodyOffsetPagination',
        '\Ensi\CustomerAuthClient\Dto\RegisterRequest',
        '\Ensi\CustomerAuthClient\Dto\ErrorResponse2',
        '\Ensi\CustomerAuthClient\Dto\MessageForResetPasswordSuccess',
        '\Ensi\CustomerAuthClient\Dto\UserResponse',
        '\Ensi\CustomerAuthClient\Dto\MessageForSetPassword',
        '\Ensi\CustomerAuthClient\Dto\SearchUsersResponseMeta',
        '\Ensi\CustomerAuthClient\Dto\User',
        '\Ensi\CustomerAuthClient\Dto\CreateTokenRequest',
        '\Ensi\CustomerAuthClient\Dto\RequestBodyOffsetPagination',
        '\Ensi\CustomerAuthClient\Dto\GrantTypeEnum',
        '\Ensi\CustomerAuthClient\Dto\SearchConfirmationCodesResponse',
        '\Ensi\CustomerAuthClient\Dto\CreateConfirmationCodeByPhoneRequest',
        '\Ensi\CustomerAuthClient\Dto\CustomerPersonalData',
        '\Ensi\CustomerAuthClient\Dto\ModelInterface',
        '\Ensi\CustomerAuthClient\Dto\UserReadonlyProperties',
        '\Ensi\CustomerAuthClient\Dto\ConfirmationCodeModel',
        '\Ensi\CustomerAuthClient\Dto\UserWriteOnlyProperties',
        '\Ensi\CustomerAuthClient\Dto\EmptyDataResponse',
        '\Ensi\CustomerAuthClient\Dto\ResponseBodyCursorPagination',
        '\Ensi\CustomerAuthClient\Dto\ErrorResponse',
        '\Ensi\CustomerAuthClient\Dto\PasswordResetSecondRequest',
        '\Ensi\CustomerAuthClient\Dto\SearchUsersResponse',
        '\Ensi\CustomerAuthClient\Dto\PaginationTypeEnum',
        '\Ensi\CustomerAuthClient\Dto\UserFillableProperties',
        '\Ensi\CustomerAuthClient\Dto\DeleteCustomerPersonalDataRequest',
        '\Ensi\CustomerAuthClient\Dto\CreateTokenResponse',
        '\Ensi\CustomerAuthClient\Dto\ConfirmationCodeResponseData',
        '\Ensi\CustomerAuthClient\Dto\PasswordResetFirstRequest',
        '\Ensi\CustomerAuthClient\Dto\SearchConfirmationCodesRequest',
        '\Ensi\CustomerAuthClient\Dto\ConfirmationCodeResponse',
        '\Ensi\CustomerAuthClient\Dto\RequestBodyPagination',
        '\Ensi\CustomerAuthClient\Dto\PaginationTypeOffsetEnum',
        '\Ensi\CustomerAuthClient\Dto\PatchUserRequest',
        '\Ensi\CustomerAuthClient\Dto\SearchUsersRequest',
        '\Ensi\CustomerAuthClient\Dto\MessageForResetPassword',
    ];

    /** @var string */
    public static $configuration = '\Ensi\CustomerAuthClient\Configuration';
}
