<?php
/**
 * ConfirmationCodeModelTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\CustomerAuthClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi Customer Auth
 *
 * Customer authorisation
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Ensi\CustomerAuthClient;

use PHPUnit\Framework\TestCase;

/**
 * ConfirmationCodeModelTest Class Doc Comment
 *
 * @category    Class
 * @description ConfirmationCodeModel
 * @package     Ensi\CustomerAuthClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class ConfirmationCodeModelTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "ConfirmationCodeModel"
     */
    public function testConfirmationCodeModel()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "user_id"
     */
    public function testPropertyUserId()
    {
    }

    /**
     * Test attribute "phone"
     */
    public function testPropertyPhone()
    {
    }

    /**
     * Test attribute "attempt"
     */
    public function testPropertyAttempt()
    {
    }

    /**
     * Test attribute "failed_checks_count"
     */
    public function testPropertyFailedChecksCount()
    {
    }

    /**
     * Test attribute "expired_at"
     */
    public function testPropertyExpiredAt()
    {
    }

    /**
     * Test attribute "verified_at"
     */
    public function testPropertyVerifiedAt()
    {
    }

    /**
     * Test attribute "created_at"
     */
    public function testPropertyCreatedAt()
    {
    }

    /**
     * Test attribute "updated_at"
     */
    public function testPropertyUpdatedAt()
    {
    }
}
